# Coding Challenges

Contained are algorithm challenges from sites such as Kattis, and Google.

## Getting Started
The problem links are included at the top of each file. The solutions can be submitted directly into the online compilers of Kattis or Google.


### Prerequisites

We used Python for all the solutions contained in this repository.
However, the exact python version required will vary depending on the compiler version for each specific site.

```
Kattis - Python 3.6
Google - Python 3.5.3 (Note - No f-strings)
```

## Completed Challenges

### Kattis

* [Out of Sorts](https://bitbucket.org/wilsonhuang92/coding-challenges/src/master/kattis/Out%20of%20Sorts.py)
* [Room Painting](https://bitbucket.org/wilsonhuang92/coding-challenges/src/master/kattis/Room%20Painting.py)
* [Firefly](https://bitbucket.org/wilsonhuang92/coding-challenges/src/master/kattis/firefly.py)
    * [Firefly v2](https://bitbucket.org/wilsonhuang92/coding-challenges/src/master/kattis/firefly%20v2.py) - More efficient than above
* [H-Index](https://bitbucket.org/wilsonhuang92/coding-challenges/src/master/kattis/H-Index.py)
* [Fosh Week](https://bitbucket.org/wilsonhuang92/coding-challenges/src/master/kattis/Fosh%20Week.py)
* [Square Pegs](https://bitbucket.org/wilsonhuang92/coding-challenges/src/master/kattis/Square%20Pegs.py)
* [Suspension Bridge](https://bitbucket.org/wilsonhuang92/coding-challenges/src/master/kattis/Suspension%20Bridge.py)
* [Assigning Workstations](https://bitbucket**.org/wilsonhuang92/coding-challenges/src/master/kattis/Assigning%20Workstations.py)



### Google
* #### 2019
    * ##### Round D
        * [X or What](https://bitbucket.org/wilsonhuang92/coding-challenges/src/master/Google%20Kickstart/2019/Round%20D/1.%20X%20or%20What.py)
        * [Latest Guests](https://bitbucket.org/wilsonhuang92/coding-challenges/src/master/Google%20Kickstart/2019/Round%20D/2.%20latest_guests.py)
        * [Food Stalls](https://bitbucket.org/wilsonhuang92/coding-challenges/src/master/Google%20Kickstart/2019/Round%20D/3.%20food_stalls.py)


## Acknowledgments

* Aviel for inspiring me to do this
* Michael for suggesting problems to do
