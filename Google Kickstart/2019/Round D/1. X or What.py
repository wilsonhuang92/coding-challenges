# Problem: https://codingcompetitions.withgoogle.com/kickstart/round/0000000000051061/0000000000161426
# Language/Version: Python 3.5.6
# Result: Both test sets passed
# Wilson Counter 3

import bisect

class StevensArray():
    def __init__(self, original_array):
        self.parity, self.odds = [], []

        for i in range(len(original_array)): # O(N)
            is_odd = get_is_odd(original_array[i])
            self.parity.append(is_odd)

            if is_odd:
                self.odds.append(i)

        self.answer = self.get_subinterval()

    def get_subinterval(self):
        len = len(self.parity)
        if not self.odds:
            return len # Case: No odd numbers. Answer is the full length of the array
        if len(self.odds) & 1 == False: # Case: Even number of odd numbers. Answer is the full length of the array
            return len

        x = len - self.odds[0] - 1
        y = self.odds[-1]
        return max(x, y)

    def perform_modification(self, p, v):
        p_parity = self.parity[p]
        v_parity = get_is_odd(v)

        if p_parity == v_parity:
            return self.answer # Answer is the same as before. Lets get out of here!
        elif v_parity == 1: # The new parity is odd
            self.parity[p] = v_parity # O(1)
            bisect.insort(self.odds, p) # O(N) but prevents multiple O(N) later
        elif v_parity == 0:  # even
            self.odds.remove(p) # O(N)! Can try to use del self.odds[x] (O(1)), but we dont know where X is for P
            self.parity[p] = v_parity

        self.answer = self.get_subinterval()
        return self.answer

def get_is_odd(value): #O(N) because of count
    parity = bin(value).count("1")
    return parity & 1


# Main program

debug = 1

if debug == 1:
    T = 1
else:
    T = int(input())
for t in range(T):

    if debug == 1: # Used purely for testing
        num_integers, num_modifications = 10, 4
        original_array = [3, 3, 3, 3, 3, 3, 3, 3, 3, 3]

        # These are test cases. Answer is commented to the side.
        modifications = [[4, 7], [8, 7], [1, 7], [1, 3]]  # 5 10 8 10
        modifications = [[5, 7], [4, 7], [2, 7], [1, 7]]  # 5 10 7 10
        modifications = [[1, 3], [9, 3], [2, 7], [1, 7]]  # 10 10 7 10
        modifications = [[0, 7], [9, 3], [2, 7], [1, 7]]  # 9 9 10 9
        modifications = [[4, 7], [5, 7], [6, 7], [7, 7]]  # 5 10 6 10
        modifications = [[7, 7], [4, 7], [1, 7], [3, 7]]  # 7 10 8 10
        modifications = [[1, 3], [1, 7], [1, 3], [1, 7]]  # 10 8 10 8

    else: # Debugging is off
        num_integers, num_modifications = [int(x) for x in input().split()]
        original_array = [int(x) for x in input().split()]

        modifications = []
        for i in range(num_modifications): # O(Num_modifications)
            modifications.append([int(x) for x in input().split()])

    # Start the fun

    stevens_array = StevensArray(original_array)
    answer = []

    for i in range(num_modifications): # O(Q) but it is necessary
        p = modifications[i][0]
        v = modifications[i][1]
        result = stevens_array.perform_modification(p, v)
        answer.append(result)

    print ("Case #{}: {}".format(t+1, ' '.join([str(x) for x in answer])))
