import logging
log = logging.getLogger('log')
logging.basicConfig(level=logging.INFO, format='%(levelname)s - %(message)s')

debug = 0

# Get the number of test cases.
if debug != 1:
    log.info("How many test cases?")
    test_cases = int(input())
else:
    test_cases = 1

# Lets start the exam!
for x in range(1,test_cases+1):

    # Lets read all the data from Google first to determine our test case
    if debug != 1:
        log.info("How many stalls and spots?")
        stalls, spots = [int(x) for x in input().split()]
        _input_x = [int(x) for x in input().split()]
        _input_c = [int(x) for x in input().split()]
    else:
        stalls, spots = [2, 4]
        _input_x = [1,2,3,10]
        _input_c = [100,70,80,20]

    # Save the data into a single data structure
    # To access, use street[key]
        # Where key = 1-based index
    street = {}
    min_cost = None
    # Test each warehouse location
    for _spots in range(1, spots+1):
        street[_spots] = {'cost_distance' : _input_x[_spots-1],
                          'cost_build' : _input_c[_spots-1]
                          }

    # For each spot, calculate the total build cost.
    for spot, values in street.items():
        warehouse_cost = values['cost_build']
        warehouse_distance = values['cost_distance']
        log.debug("Warehouse Cost: {}. Warehouse Distance: {}".format(warehouse_cost, warehouse_distance))

        # street_relative_cost = {} # This can actually just be a list, since we dont care about N anymore
        street_relative_cost_list = []
        for open_spot, open_value in street.items():
            if open_spot == spot:
                pass
            else:
                relative_cost = open_value['cost_build']
                relative_cost += abs(warehouse_distance - open_value['cost_distance'])
                # street_relative_cost[open_spot] = relative_cost
                street_relative_cost_list.append(relative_cost)

        street_relative_cost_list.sort()
        sum_relative_cost = 0
        for relative_cost in street_relative_cost_list[0:stalls]:
            sum_relative_cost += relative_cost

        total_cost = sum_relative_cost+warehouse_cost
        if min_cost == None:
            min_cost = total_cost
        elif total_cost<min_cost:
            min_cost = total_cost

        log.debug(street_relative_cost_list)



    str = "Case #{}: {}".format(x, min_cost)
    print(str)
