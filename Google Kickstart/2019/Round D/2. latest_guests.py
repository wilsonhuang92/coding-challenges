# This is the brute force solution of latest guests.
# Wilson Counter 1


import logging
log = logging.getLogger('log')
logging.basicConfig(level=logging.DEBUG, format='%(levelname)s - %(message)s')


def get_next_consulate(current_person, current_consulate):
    direction = guests_direction[current_person]

    # If 1, then check for position 5
    # If -1, then check for position 1
    if direction == 1:
        if current_consulate == N: # We are at the last consulate. Return to beginning!
            return 1
        else:
            return current_consulate + direction
    if direction == -1:
        if current_consulate == 1: # We are at the first consulate, and we are moving backwards!
            return N
        else:
            return current_consulate + direction

log.info("How many test cases?")
T = int(input())

# For each test case..
for t in range(1, T+1):
    # N = # of Consulates    G + # of Guests    M = # of Minutes
    log.info("CASE #{}: What is N, G and M?".format(t))
    N, G, M = [int(x) for x in input().split()]
    log.debug("N: {}, G:{}, M:{}".format(N,G,M))

    Guest = []
    for _g in range(G):
        log.info("CASE #{}: Input guest information #{}".format(t, _g+1))
        Guest.append(input().split())
    log.info("CASE #{}: Variables Set:".format(t))
    log.info("N: {}, G:{}, M:{}".format(N,G,M))
    log.info("Total Guest Information: {}".format(Guest))

    # Lets initialize an dictionary to store starting positions
    consulates_map_start = {}
    guests_direction = {}
    consulates_memory_storage = {}

    # For each Guest (G), place them in to their starting positions.
    for x in range(1, G+1):

        startCon, direction = Guest[x-1]
        startCon = int(startCon)

        # Appending into an array, because more than one person can be here
        consulates_map_start.setdefault(startCon, []).append(x)

        direction = direction.upper()
        if direction == 'A':
            direction = -1
        elif direction == 'C':
            direction = 1
        guests_direction[x] = direction

    log.info("Starting Consulates: {}".format(consulates_map_start))

    consulates_memory_storage = consulates_map_start # This is necessary, as we need to store the initial visit too!

    # Now we need to iterate through each round, and save the end state of the consulates, of who they remember.
    for x in range(1, M+1):
        # Whats this for?
        # consulates_map_step = {new_list: [] for new_list in range(4)}
        # print(f"Start New Consulates: {consulates_map_step}")
        consulates_map_step = {}

        # Simulate a round. Save the new end state
        # Keep in mind that if a person already exists here, add them to the dictionary, not remove
        log.debug("Start to simulate movement.")
        for consulate in consulates_map_start:
            log.debug("--Consulate {}:".format(consulate))
            for person in consulates_map_start[consulate]:
                next_consulate = get_next_consulate(person, consulate)

                if next_consulate in consulates_map_step:
                    consulates_map_step[next_consulate].append(person)
                else:
                    consulates_map_step[next_consulate] = [person]
                # consulates_map_step.setdefault(next_consulate, []).append(person) # too fancy for this nub

                log.debug("----Person {} was at {} and moved to {}:".format(person, consulate, next_consulate))

            log.debug("----Step looks like: {}".format(consulates_map_step))




        # Compare starting state against current state
        # If a consulate has a new visitor, then replace that visitor. If not, then keep the old visitor

        # First, we need to delete unused keys for the sake of our merge
        for _key in list(consulates_map_step):
            if consulates_map_step[_key] == []:
                del consulates_map_step[_key]

        log.debug("----Consulates started as {}".format(consulates_map_start))
        log.debug("----Consulates is now as {}".format(consulates_map_step))


        consulates_memory_storage = {**consulates_memory_storage, **consulates_map_step}

        log.debug("----Lets merge them! {}".format(consulates_memory_storage))

        # Reset! Our new starting position is where we last ended off.
        consulates_map_start = consulates_map_step

        # This link may help
        # https://stackoverflow.com/questions/38987/how-do-i-merge-two-dictionaries-in-a-single-expression-in-python

    output = {}
    # this is cheating, but oh well for now..
    # if not bool(consulates_memory_storage):
    #     consulates_memory_storage = consulates_map_start

    for _x in consulates_memory_storage:
        for _y in consulates_memory_storage[_x]:
            output[_y] = output.get(_y, 0)+1

    f_text = []
    for _x in output:
        f_text.append(output[_x])

    f_text = ' '.join([str(elem) for elem in f_text])
    f_text = "Case #{}: {}".format(t,f_text)
    print (f_text)



