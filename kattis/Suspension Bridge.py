"""
Problem URL: https://open.kattis.com/problems/suspensionbridges

Results
=======
    [Passed]
    CPU Time: 0.05 Seconds

Explanation of Solution
=======================
    To use the bisection method, we must find the interval at which there is a sign change of function f(x) and f(y).
    We assume that if a=0, f(x) is always negative.
    So, we must find a where f(y) is positive.
    We implement this by starting off with a=d**2.
    If f(y) is still negative, we continue a=a**2.

    Then, we run a resursive binary search tree to find where f(z) = 0

    Using f(z), we can calculate l.


Notes
=====
Author: Wilson Huang
Create Date: 05/25/2020
Modified Date: 05/25/2020
? O(N):
"""
import math


d,s = [int(x) for x in input().split()]

def f(s, d, a):
    """ This is a implementation of the bisection method """
    f = (a+s) - (a*math.cosh(d/(2*a)))
    return f

fa = f(s, d, 1)

max = d
fb = -1

while fb < 0:
    max = max ** 2
    fb = f(s,d,max)


def find_0(min, max):
    mid = (min+max)/2

    o = f(s,d,mid)
    o = round(o,4)

    if o == 0:
        return mid
    if o > 0:
        return find_0(min, mid)
    if o < 0:
        return find_0(mid, max)

def calc_l(a,d):
    return 2*a*math.sinh(d/(2*a))

a = find_0(0, max)
l = calc_l(a,d)

print(l)



