"""
Problem URL: https://open.kattis.com/problems/hindex/

Results
=======
    [Passed]
    CPU Time: 0.18 Seconds

Explanation of Solution
=======================
    To calculate h-index, we first need to sort the values of h in descending order.
    We then look for the position of h where the value is greater than or equal to the position.

    For example:
    H-Index -> [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
    Position-> [1,  2, 3, 4, 5, 6, 7, 8, 9,10]
                             ^
    The answer is 5, because it is the last H-Index where the value >= the position.

    To solve this in Python, we use a recursion with the exit condition where:
        h-index[position] < position AND
        h-index[position-1] > position - 1

    It is important to note that H-Index is based on a 1-index scale.
    Position, on the other hand, is based on a 0-index scale.
    However, since our recursion finds the position+1, the result of the recursion is the correct H-Index


Notes
=====
Author: Wilson Huang
Create Date: 05/22/2020
Modified Date: 05/22/2020
? O(N):
"""

import math

def find_hindex(min, max, h_index):
    mid = math.floor((min+max)/2)
    int = h_index[mid] - 1
    int_2 = h_index[mid-1] - 1

    if min>max:
        return max+1
    if int < mid and int_2 >= mid-1:
        return mid
    if int < mid:
        return find_hindex(min, mid-1, h_index)
    if int >= mid:
        return find_hindex(mid+1, max, h_index)

n = int(input()) # papers written

h_index = []
for i in range(n):
    h_index.append(int(input()))
h_index.sort(reverse=True)

pos = find_hindex(0, len(h_index)-1, h_index)

print(pos)