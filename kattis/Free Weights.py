# https://open.kattis.com/problems/freeweights


'''
Strategy

Read rows 1 and 2
Into a list [x,y]
Where x is the weight, and y is the position starting from 0

Options:
Start with the lowest weight out of position
0) What is the position of the individual weights that need to be rearranged?
    Implementation:
    Loop through the rows. Check if they are in pairs. If not, add them into a list.

1) If they are beside each other, with free spaces in between, roll them together!
    Dont increase MAX weight count
    Implementation:
        x = weight
        i = position of x
        bool = True
        i=0
        j=1

        while bool == True:
            if f(i+j) == 0:
                j += 1
                continue
            elif: f(i+j) != x:
                break

            if f(x+1) == x:
                Roll f(i) j times at cost of 0

            break or bool==false


2) Else:
    Is there a free space beside either pair? (position +/- free?) If pos ==0 or len, then it is free
    If so, move it there
3) We will need to move both weights to an end cap.

    Implementation:
        x = weight
        i = position of weight 1
        j = position of weight 2
        ri = row of i
        rj = row of j


        if i = 0 or i=len(rowi):
            Move j to ri[0] or ri[-1]
        elif j = 0 or j=len(rowj):
            Move j to rj[0] or rj[-1]
        elif
            move both i and j to r1[-1]





Other thoughts
* For efficiency, it would probably be best to keep a dictionary of the positions
* Keep a list of status?
    0 = free space
    -1 = matched
    * = not matched



'''

