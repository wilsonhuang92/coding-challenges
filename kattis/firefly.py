"""
Problem URL: https://open.kattis.com/problems/firefly

Results
=======
    [Passed] [TLE]

Explanation of Solution
=======================
    This is a simulation solution. See [Firefly v2.py] for a more efficient solution (histogram approach).

    In this script, we first build a list of length H, filled with the values of 0.
    Then for each obstacle, we identify if it is a stalagmite or stalactite.
    If it is a stalagmite, we loop through the list, starting from the 0 position, and add 1 to the count, for h
    If it is a stalactite, we loop through the list, starting from the end position, and add 1 to the count, for h

Notes
=====
Author: Wilson Huang
Create Date: 05/21/2020
Modified Date: 05/21/2020
? O(N*M)
"""

obstacles = []
N, H = [int(x) for x in input().split()]
for i in range(N):
    obstacles.append(int(input()))

histogram = [0] * H
for i in range(N):
    stala = obstacles[i]

    for y in range(H):
        if i & 1 == False:
            z = y
        else:
            z = -(y+1)
        if stala > 0:
            histogram[z] += 1
            stala -= 1

        if stala == 0:
            break
histogram.sort()
min = histogram[0]
count = 0
for i in histogram:
    if i == min:
        count += 1
    else:
        break
print("{} {}".format(min, count))





