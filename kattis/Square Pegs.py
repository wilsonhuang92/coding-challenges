"""
Problem URL: https://open.kattis.com/problems/squarepegs

Results
=======
    [Passed]
    CPU Time: 0.09 Seconds

Explanation of Solution
=======================
    In this solution, we will make a single list of radius for both round and square houses.
    We then sort the radius, and the plots of lands, in ascending order.
    We can now use bisect to find the index of land that is the left most but less than x.

    Do not consider anything left of X+1 once it has been used.
    Once we cannot find a plot big enough, exit the for loop altogether.


Notes
=====
Author: Wilson Huang
Create Date: 05/25/2020
Modified Date: 05/25/2020
? O(N):
"""


import bisect
import logging
import math

log = logging.getLogger('log')
logging.basicConfig(level=logging.DEBUG, format="%(message)s")

def get_radius_of_square(x):
    x = float(x)
    radius = ((x * 0.5) ** 2) * 2
    radius = round(radius,2)
    return math.sqrt(radius)


n, m, k = [int(x) for x in input().split()]
radius_land = [int(x) for x in input().split()]
radius_house = [float(x) for x in input().split()]
radius_house.extend([get_radius_of_square(x) for x in input().split()])


radius_house.sort()
radius_land.sort()
track, count = 0, 0
for x in range(m+k):
    size = radius_house[x]

    log.debug(size)
    log.debug(radius_land[track:])
    pos = bisect.bisect_right(radius_land, size, track)

    if pos == len(radius_land):
        break

    count += 1
    track = pos+1


print (count)



