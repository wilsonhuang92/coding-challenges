"""
Problem URL: https://open.kattis.com/problems/firefly

Results
=======
    [Passed]

Explanation of Solution
=======================
    This is a efficient histogram solution. See [Firefly.py] for a simulation solution.

    In this attempt, we will create two lists, one for the stalagmites and the other for the stalactites.
    Using these lists, we create two histograms, based on the height of each stalagmites and stalactites.
    We then transform the histograms into two cumulative histograms to depict how many obstacles the firefly will fly through at each height.
    And lastly, if we add the two histograms together, this will tell us the number of obstacles the firefly will hit at each height.

Notes
=====
Author: Wilson Huang
Create Date: 05/21/2020
Modified Date: 05/21/2020
? O(N)
"""

N, H = [int(x) for x in input().split()]
stalag = [0] * H
stalac = [0] * H

for i in range(N):
    y = int(input())
    if i & 1 == 0: # (Even)
        stalag[y-1] += 1
    else:
        stalac[H-y] += 1

sum = 0
for i in range(H-1,0-1,-1):
    sum += stalag[i]
    stalag[i] = sum

sum = 0
best = float('inf')
count = 0


for i in range(H):
    sum += stalac[i]
    stalac[i] = sum

    ## Comparision
    ouch = stalag[i] + stalac[i]
    if ouch < best:
        best = ouch
        count = 0

    if ouch == best:
        count +=1

print (best, count)





