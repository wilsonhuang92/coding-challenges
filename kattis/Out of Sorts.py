"""
Problem URL: https://open.kattis.com/problems/outofsorts

Results
=======
    [Passed]
    CPU Time: 0.94 Seconds

Explanation of Solution
=======================
    This is a simple binary search.
    x[] represents an unsorted list.
    For each x in x[], perform a binary search.
    Output the number of integers found after performaing a binary seach on each integer.


Notes
=====
Author: Wilson Huang
Create Date: 05/20/2020
Modified Date: 05/20/2020
? O(N):
"""

import math
def binary_search(i, low, high):
    mid = math.floor((low + high) / 2)
    if low > high:
        return None
    if i < x[mid]:
        return binary_search(i, low, mid-1)
    elif i > x[mid]:
        return binary_search(i, mid+1, high)
    elif i == x[mid]:
        return mid

input = input()
n, m, a, c, x0 = [int(x) for x in input.split()]
x = [x0]

for i in range(n):
    x.append((a*x[i]+c) % m)

x = x[1:]

found_set = 0

for i in x:
    pos = binary_search(i,0,len(x)-1)
    if pos is not None:
        found_set += 1


print (found_set)