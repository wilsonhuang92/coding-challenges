"""
Problem URL: https://open.kattis.com/problems/roompainting

Results
=======
    [Passed]
    CPU Time: 0.37 Seconds

Explanation of Solution
=======================
    We are trying to find the number of ML wasted when purchasing paint cans.
    Do this by first finding the modulo where required ML > max can size. There will never be any wastage on full cans.

    We could do a binary search like this... (Note that we did not do this method, as it is not required)
    Taking the list of required paint cans, we search for a paint can size that is equal to, or the first larger paint can than is required.
    This is accomplished by using recursion with an exit condition of:
        Paint_Can_ML[index] >= Required ML AND
        Paint_Can_ML[index-1] < Required ML

    Instead, this problem was simple enough to simply loop through the paint can selection to find the first one that is equal or greater than the required size.

Notes
=====
Author: Wilson Huang
Create Date: 05/21/2020
Modified Date: 05/21/2020
? O(N):
"""
#

cans_offered, colors_required = [int(x) for x in input().split()]

can_sizes = []
max_can_size = 0

for i in range(cans_offered):
    can_size_int = int(input())
    can_sizes.append(can_size_int)
    # Lets store our max can size.
    if can_size_int > max_can_size:
        max_can_size = can_size_int

can_sizes.sort()
wastage = 0

amount_needed = []
for i in range(colors_required):
    amount_needed_int = int(input())
    if amount_needed_int > max_can_size:
        amount_needed_int = amount_needed_int % max_can_size
    if amount_needed_int > 0:
        for j in range(len(can_sizes)):
            if amount_needed_int <= can_sizes[j]:
                wastage += can_sizes[j] - amount_needed_int
                break

print (wastage)




