"""
Problem URL: https://open.kattis.com/problems/froshweek2

Results
=======
    [Passed]
    CPU Time: 0.18 Seconds

Explanation of Solution
=======================
    Sort both tasks and time intervals in ascending order.
    With sorted Tasks, we can loop through sorted Time Intervals to find the first interval that allows for that task.
    The next search can ignore all Time Intervals that are before the last found Time Interval, as they are guaranteed to be too small.

    Further, when we reach the end of Time Interval, we can quit the search altogether.

Notes
=====
Author: Wilson Huang
Create Date: 05/24/2020
Modified Date: 05/24/2020
? O(N):
"""

import bisect
import logging

log = logging.getLogger('log')
logging.basicConfig(level=logging.INFO, format="%(message)s")


debug = 2

if debug == 0:
    n, m = [int(x) for x in input().split()]
    t = [int(x) for x in input().split()]
    l = [int(x) for x in input().split()]
if debug == 2:
    n, m = [int(x) for x in "4 4".split()]
    t = [int(x) for x in "180000 185000 199999 100000".split()]
    l = [int(x) for x in "199999 180000 170000 120000".split()]
if debug == 3:
    n, m = [int(x) for x in "3 3".split()]
    t = [int(x) for x in "199999 180000 170001".split()]
    l = [int(x) for x in "199999 170000 180000".split()]


t.sort()
l.sort()
track = 0
count = 0

for i in range(n):
    time = t[i]

    # log.debug("For {} in {}. Find position in {}".format(time, t, l[track:]))

    pos = bisect.bisect_left(l, time, track)
    if pos != len(l):
        count +=1
        track = pos+1
    else:
        break # we arent going to find anything new.

    log.debug(pos)



print (count)