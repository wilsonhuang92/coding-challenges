"""
Problem URL: https://open.kattis.com/problems/workstations

Results
=======
    [Passed]
    CPU Time: 0.51 seconds

Explanation of Solution
=======================
    We save the researchers arrivals and departures independently, and sort the two.

    Looping through the researchers, we use a queue to determine if the workstation is unlocked.

Notes
=====
Author: Wilson Huang
Create Date: 06/03/2020
Modified Date: 06/08/2020
? O(N):
"""

arrivals = []
departures = []

n, m = [int(x) for x in input().split()]
for _ in range(n):
    l = [int(x) for x in input().split()]
    arrivals.append(l[0])
    departures.append(l[0]+l[1])

arrivals.sort()
departures.sort()
count,i = 0

for arrival_time in arrivals:

    while arrival_time > departures[i]+m:
        i += 1
    if arrival_time >= departures[i]:
        i += 1
        count += 1

print(count)


