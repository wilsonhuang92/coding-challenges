"""
Problem URL: https://open.kattis.com/problems/monk

Results
=======
    [Passed]
    CPU Time: 0.29 Seconds

Explanation of Solution
=======================
    We can represent Speed as (Distance_X2 - Distance_X1) / (Time_X2 - Time_X1)
    Using Speed, we can calculate the exact point the monk will be at a specific point in time.
        Point = Speed * Time

    Using a binary search tree, we find the earliest time where ElevationAsc = ElevationDesc.

Notes
=====
Author: Wilson Huang
Create Date: 05/23/2020
Modified Date: 05/24/2020
? O(N):

"""
import math
import logging
import bisect
log = logging.getLogger('log')
logging.basicConfig(level=logging.CRITICAL, format='%(message)s')

debug = 3
total_elevation = 0

if debug == 0:
    a, d = [int(x) for x in input().split()]
    asc = []
    desc = []
    for i in range(a):
        asc.append([int(x) for x in input().split()])
        total_elevation += asc[-1][0]

    for i in range(d):
        desc.append([int(x) for x in input().split()])
elif debug == 1:
    total_elevation += 10

    a, d = 1, 1
    asc = [[10, 11]]
    desc = [[10, 10]]
elif debug == 2:
    total_elevation += 10

    a, d = 3, 1
    asc =[[4, 2], [0, 3], [6, 3]]
    desc = [[10, 7]]
elif debug == 3:
    total_elevation += 5

    a, d, = 3, 3
    # Lists are represented as h, t
    # h is elevation, t is time
    asc = [[2, 3], [0, 5], [3, 1]]
    desc = [[3, 4], [0, 2], [2, 2]]


# Main Program Start
log.debug("Asc: {} \nDesc:{}".format(asc, desc))
# We should change everything to cumulative elevation and time


_e, _t = 0,0
asc_keys = []
for i in range(len(asc)):
    _e += asc[i][0]
    _t += asc[i][1]
    asc[i] = [_e, _t]
    asc_keys.append(_t)

asc.insert(0, [0,0])
asc_keys.insert(0, 0)


_e, _t = total_elevation, 0

desc_keys = []
_temp_desc = []
for i in range(len(desc)):
    _e -= desc[i][0]
    _t += desc[i][1]
    _temp_desc.append([_e, _t])
    desc_keys.append(_t)

desc = _temp_desc
desc.insert(0,[total_elevation, 0])
desc_keys.insert(0, 0)

max_time = min(asc[-1][1], desc[-1][1])


# Go to the middle time, and check the elevation. Binary search tree until elevation is same
# Nvm, we should check elevation, not time.

def get_distance(f_time, list, list_keys):
    i = bisect.bisect_left(list_keys, f_time)
    if i:
        i -= 1

    speed = (list[i + 1][0] - list[i][0]) / (list[i + 1][1] - list[i][1])
    log.debug("\t\tIndex = {} ".format(i))
    log.debug("\t\tSpeed = {} ".format(speed))

    distance = (f_time - list[i][1]) * speed + list[i][0]
    distance = round(distance, 7)
    return distance


def BST(min, max):
    f_time = (min+max)/2
    log.info(f"Min: {min}.  Max: {max}.  f_time:{f_time}")

    # ASC get elevation at time
    log.debug("\nAscending\n\tCumulative: {}\n\tAsc Elevation Keys: {}".format(asc, asc_keys))

    distance_asc = get_distance(f_time, asc, asc_keys)
    log.info("Asc distance at time {}s = {}m ".format(f_time, distance_asc))

    # DESC get earliest time at elevation
    log.debug("\nDescending\n\tCumulative: {}\n\tDesc Elevation Keys: {}".format(desc, desc_keys))

    distance_desc = get_distance(f_time, desc, desc_keys)
    log.info("Desc distance at time {}s = {}m ".format(f_time, distance_desc))



    if distance_asc == distance_desc:
        offset = 0.000001
        if get_distance(f_time-offset, asc, asc_keys) ==  get_distance(f_time-offset, desc, desc_keys):
            return BST(min, f_time-offset)

        return f_time

    if distance_asc < distance_desc:
        return BST(f_time, max)

    if distance_asc > distance_desc:
        return BST(min, f_time)



x = BST(0, max_time)
x = round(x,7)

print(x)